from math import floor

class PrimeNumberDigit(object):
    def __init__(self, digit):
        self.prime_numbers = []  # list of prime numbers
        self.prime_digits = []  # list of all prime numbers that contain the given digit
        self.digit = digit
        self.current = digit - 1  # -1 so that we start with given digit as smallest prime

    def __iter__(self):
        return self

    def __next__(self):
        number = self.current + 1
        while True:
            is_prime = self.is_prime(number)

            if is_prime:
                # check if number has given digit
                if str(self.digit) in str(number):
                    self.prime_digits.append(number)
                    self.prime_numbers.append(number)
                    self.current = number
                    break
                else:
                    self.prime_numbers.append(number)
                    number += 1
            else:
                number += 1
        print(self.current)

    def is_prime(self, number):
        if (number <= 1):
            return False
        if (number == 2):
            return True

        while True:
            for div in range(2, floor(number/2) + 1):
                if number % div == 0:
                    number = number + 1
                    return False
            return True

div = PrimeNumberDigit(6)
next(div)
next(div)
next(div)
next(div)
next(div)
next(div)