from time import time

def timing_decorator(func):
	def new_func(*args, **kwargs):
		start = time()
		func(*args, **kwargs)
		end = time()
		print("Time it took to run the function: {0:.2f}ms".format((end - start)*1000))
	return new_func

@timing_decorator
def adding_function(x, y):
	print("The sum is {}.".format(x+y))

adding_function(3, 5)