username = input("Enter username: ")
begingame = input("Begin game[y/n]? ")
import random
from datetime import datetime
questionslist = ["%d + %d = ", "%d - %d = ", "%d * %d = ", "%d / %d = "]
highscore = dict()
while username != "":
	if begingame.lower() == "y":
		starttime = datetime.now()
		pointtally = 0
		errortally = 0
		existinguser = 0
		playerhighscore = 0
		playererror = 0
		for x in range(1, len(highscore)+1):
			if highscore[x].get('username') == username:
				#print('existing user')
				existinguser = x
				playerhighscore = highscore[x].get('score')
				playererror = highscore[x].get('errors')
				break
		while True:
			randomquestion = random.randint(0, 3)
			if randomquestion == 0:
				randomnumber1 = random.randint(10, 99)
				randomnumber2 = random.randint(10, 99)
				useranswer = input(questionslist[randomquestion] % (randomnumber1, randomnumber2))
				if useranswer == (randomnumber1 + randomnumber2):
					#add points
					print("correct")
					pointtally += 1
				else:
					#add errors
					print("wrong")
					errortally += 1
			elif randomquestion == 1:
				randomnumber1 = random.randint(10, 99)
				randomnumber2 = random.randint(10, 99)
				useranswer = input(questionslist[randomquestion] % (randomnumber1, randomnumber2))
				if useranswer == (randomnumber1 - randomnumber2):
					#add points
					print("correct")
					pointtally += 1
				else:
					#add errors
					print("wrong")
					errortally += 1
			elif randomquestion == 2:
				randomnumber1 = random.randint(10, 99)
				randomnumber2 = random.randint(2, 9)
				useranswer = input(questionslist[randomquestion] % (randomnumber1, randomnumber2))
				if useranswer == (randomnumber1 * randomnumber2):
					#add points
					print("correct")
					pointtally += 1
				else:
					#add errors
					print("wrong")
					errortally += 1
			else:
				#add division here
				randomnumber1 = random.randint(100, 999)
				randomnumber2 = random.randint(2, 9)
				while True:
					if randomnumber1 % randomnumber2 != 0:
						randomnumber2 = random.randint(2, 9)
					else:
						break
				useranswer = input(questionslist[randomquestion] % (randomnumber1, randomnumber2))
				if useranswer == (randomnumber1 / randomnumber2):
					#add points
					print("correct")
					pointtally += 1
				else:
					#add errors
					print("wrong")
					errortally += 1
			if pointtally >= 10 or errortally >= 10:
				break
		endtime = datetime.now()
		totaltime = endtime - starttime
		minutes = totaltime.total_seconds()/60
		if minutes >= 1:
			seconds = totaltime.total_seconds()-(minutes*60)
		else:
			seconds = totaltime.total_seconds()
		print("Score: %d\nNumber of errors: %d\nTime: %d:%d") % (pointtally, errortally, minutes, seconds)
		if existinguser != 0:
			if pointtally > playerhighscore:
				highscore[existinguser] = {"username": username, "score": pointtally, "errors": errortally, "time": "%d:%d" % (minutes, seconds)}
		else:
			highscore[len(highscore)+1] = {"username": username, "score": pointtally, "errors": errortally, "time": "%d:%d" % (minutes, seconds)} 
		username = input("Enter username: ")
		if username == '':
			#for key, value in highscore.items():
			#	print("Key: {}\nValue: {}\n\n".format(key,value))
			for key, value in highscore.items():
				print("Number {}: {}".format(key, value))
	else:
		print("Ending program")