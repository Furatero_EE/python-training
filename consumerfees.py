import os
import glob
import string
import random
import smtplib

from datetime import datetime


def get_random_accounts():
	path = "/home/fuhrar/Python Training/python-training/Locations"
	extension = "csv"
	os.chdir(path)
	result = glob.glob('*.{}'.format(extension))
	location_lst = []
	account_number_lst = []
	consumption_lst = []
	dictionary = {}

	for x in range(3):
		random_string = ""
		random_location = result[random.randint(0, len(result) - 1)]

		for z in range(2):
			random_string += string.ascii_uppercase[random.randint(0, len(string.ascii_uppercase) - 1)]

		with open(random_location) as f:
			for i, line in enumerate(f):
				lst = [z.strip() for z in line.split(",")]
				if random_string in lst[0]:
					account_number_lst.append(lst[0])
					consumption_lst.append(lst[1])
					location_lst.append(random_location[:-4])

	dictionary['account_number'] = account_number_lst
	dictionary['location'] = location_lst
	dictionary['consumption'] = consumption_lst

	return dictionary


def compute_total_fee(dictionary):
	path = "/home/fuhrar/Python Training/python-training"
	extension = "csv"
	os.chdir(path)
	rates = glob.glob('*.{}'.format(extension))
	total_fee_lst = []

	with open(rates[0]) as f:
		for i, line in enumerate(f):
			if i != 0:
				lst = [z.strip() for z in line.split(",")]
				if lst[0].lower() in dictionary['location']:
					for index1, location in enumerate(dictionary['location']):
						if location == lst[0].lower():
							#print(index)
							for index2, consumption in enumerate(dictionary['consumption']):
								if index2 == index1:
									total_fee_lst.append(float(consumption) * float(lst[1]))

	dictionary['total_fee'] = total_fee_lst

	return dictionary


def send_email_report(dictionary):
	gmail_user = "you@gmail.com"
	gmail_password = "P@ssword!"
	sent_from = GMAIL_USER
	to = ["me@gmail.com", "bill@gmail.com"]
	subject = datetime.today().strftime("%b-%d-%Y")
	email_text = dictionary['account_number']


report = compute_total_fee(get_random_accounts())
print(report)