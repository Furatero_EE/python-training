def multiplicationtable(*args):
	if len(args) < 1 or len(args) > 2:
		print("Parameters should not be less than 1 or more than 2")
	else:
		if len(args) == 1:
			ratio = []
			for arg in args:
				ratio.append(arg)
			row = []
			for x in range(1, ratio[0] + 1):
				column = []
				for y in range(1, ratio[0] + 1):
					column.append(x * y)
				row.append(column)
			for i in row:
				for j in i:
					print(j, end = '\t')
				print('\n')
		else:
			ratio = []
			for arg in args:
				ratio.append(arg)
			i = 0
			grid = []
			for row in range(1, ratio[i+1]+1):
				lst = []
				for column in range(1, ratio[i]+1):
					lst.append(row * column)
				grid.append(lst)
			for rowitem in grid:
				for columnitem in rowitem:
					print(columnitem, end = '\t')
				print('\n')