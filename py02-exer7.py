import unittest
from exer7 import BankAccount

class Exer7TestDeposit(unittest.TestCase):
	"""Tests for 'exer7.py'"""
	def testDeposit(self):
		bank_account = BankAccount('edgar', 1, 1000)
		self.assertEqual(bank_account.deposit(1000), "Balance: 2000")
		#self.assertEqual(bank_account.deposit('1000'), "Balance: 10002000") #unsupported operand type
		#self.assertEqual(bank_account.withdraw(1000), "Balance: 0")

class Exer7TestWithdraw(unittest.TestCase):
	def testWithdraw(self):
		bank_account = BankAccount('edgar', 1, 1000)
		self.assertEqual(bank_account.withdraw(1000), "Balance: 0")
		#self.assertEqual(bank_account.withdraw('1000'), "Balance: ") #unsupported operand type
		#self.assertEqual(bank_account.withdraw(2000), "Withdraw amount is larger than current balance")

if __name__ == "__main__":
	unittest.main()