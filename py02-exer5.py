import re
regex = r"^(?!.*(\d)(-?\1){3})[456]([\d]{15}|[\d]{3}(-[\d]{4}){3})$"
is_valid_credit_card_number = lambda number: True if re.match(regex, str(number)) else False

print(is_valid_credit_card_number("1234-5678-9012-3456"))