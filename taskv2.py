#save data to text file, not dictionary
#remove dictionaries
#create user class that stores the user information(score, error, time)
#tranform all transactions to functions

import random
from datetime import datetime

class User(object):
	def __init__(self, name, begin):
		self.name = name
		self.begin = begin
	def setScore(self, score):
		self.score = score
	def setErrors(self, errors):
		self.errors = errors
	def setTime(self, minutes, seconds):
		self.minutes = minutes
		self.seconds = seconds
	def getUsername(self):
		return self.name
	def getBegin(self):
		return self.begin
	def getScore(self):
		return self.score
	def getErrors(self):
		return self.errors
	def getTime(self):
		return self.time
	@classmethod
	def from_input(cls):
		return cls(
			input('Enter username: '),
			input('Begin game[y/n]? ')
		)

def saveToText(name, score, error, minutes, seconds):
	f = open("highscores.txt", "a")
	time = minutes+":"+seconds
	stringlist = [name, score, error, time]
	for string in stringlist:
		f.write(string+" ")
	f.write("\n")
	f.close()

def startGame():
	name = ''
	begin = ''
	while True:
		user = User.from_input()
		if user.getUsername() == '':
			print('Invalid username, enter again')
		else:
			name = user.getUsername()
			begin = user.getBegin()
			break
	if begin.lower() != 'y':
		print('Terminating program')
	else:
		questionslist = ["%d + %d = ", "%d - %d = ", "%d * %d = ", "%d / %d = "]
		while name != "":
			starttime = datetime.now()
			pointtally = 0
			errortally = 0
			while True:
				randomquestion = random.randint(0, 3)
				if randomquestion == 0:
					randomnumber1 = random.randint(10, 99)
					randomnumber2 = random.randint(10, 99)
					useranswer = int(input(questionslist[randomquestion] % (randomnumber1, randomnumber2)))
					if useranswer == (randomnumber1 + randomnumber2):
						#add points
						print("correct")
						pointtally += 1
					else:
						#add errors
						print("wrong")
						errortally += 1
				elif randomquestion == 1:
					randomnumber1 = random.randint(10, 99)
					randomnumber2 = random.randint(10, 99)
					useranswer = int(input(questionslist[randomquestion] % (randomnumber1, randomnumber2)))
					if useranswer == (randomnumber1 - randomnumber2):
						#add points
						print("correct")
						pointtally += 1
					else:
						#add errors
						print("wrong")
						errortally += 1
				elif randomquestion == 2:
					randomnumber1 = random.randint(10, 99)
					randomnumber2 = random.randint(2, 9)
					useranswer = int(input(questionslist[randomquestion] % (randomnumber1, randomnumber2)))
					if useranswer == (randomnumber1 * randomnumber2):
						#add points
						print("correct")
						pointtally += 1
					else:
						#add errors
						print("wrong")
						errortally += 1
				else:
					#add division here
					randomnumber1 = random.randint(100, 999)
					randomnumber2 = random.randint(2, 9)
					while True:
						if randomnumber1 % randomnumber2 != 0:
							randomnumber2 = random.randint(2, 9)
						else:
							break
					useranswer = int(input(questionslist[randomquestion] % (randomnumber1, randomnumber2)))
					if useranswer == (randomnumber1 / randomnumber2):
						#add points
						print("correct")
						pointtally += 1
					else:
						#add errors
						print("wrong")
						errortally += 1
				if pointtally >= 10 or errortally >= 10:
					break
			endtime = datetime.now()
			totaltime = endtime - starttime
			minutes = totaltime.total_seconds()/60
			if minutes >= 1:
				seconds = totaltime.total_seconds()-(minutes*60)
			else:
				seconds = totaltime.total_seconds()
			print("Score: %d\nNumber of errors: %d\nTime: %d:%d" % (pointtally, errortally, minutes, seconds))
			user.setScore(pointtally)
			user.setErrors(errortally)
			user.setTime(minutes, seconds)
			save = saveToText(name, str(pointtally), str(errortally), str(int(minutes)), str(int(seconds)))
			user = User.from_input()
			name = user.getUsername()
		print("Terminating program")