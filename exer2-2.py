number = int(input("Enter a number: "))
if number > 5:
	print("Sum is: " + str(sum([x for x in range(1, number+1) if x % 3 == 0 or x % 5 == 0])))
else:
	print("Number must be greater than 5!")