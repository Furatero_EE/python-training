class BankAccount(object):
	interest_rate = 0.3
	def __init__(self, name, number, balance):
		self.name = name
		self.number = number
		self.balance = balance
	def deposit(self, amount):
		self.balance = self.balance + amount
		return str('Balance: %d' % self.balance)
	def withdraw(self, amount):
		totalamount = self.balance - amount
		if totalamount < 0:
			return str('Withdraw amount is larger than current balance')
		else:
			self.balance = totalamount
			return str('Balance: %d' % self.balance)
	def add_interest(self):
		interest = self.balance * self.interest_rate
		self.balance = self.balance + (self.balance * self.interest_rate)
		print('Interest rate is %d%%\nInterest is: %d\nBalance after interest: %d' % (self.interest_rate*10, interest, self.balance))

class StudentAccount(BankAccount):
	def withdraw(self, amount):
		limit = -1000
		totalamount = self.balance - amount
		if totalamount < limit:
			print('Withdraw amount exceeds the current balance + limit')
		else:
			self.balance = totalamount
			print('Balance: %d' % self.balance)