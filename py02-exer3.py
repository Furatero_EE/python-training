class Error(Exception):
	"""Base class for other exceptions"""
	pass
class HexFormatError(Error):
	"""Raised if the string does not fit with the format for hexadecimal"""
	pass
class MemoryError(Error):
	"""Raise if the string is beyond 4-bytes (8 hex digits)"""
	pass

import string

while True:
	try:
		#convert hexadecimal here
		userinput = input("Enter hexadecimal string: ")
		if len(userinput) >= 9:
			raise MemoryError

		if all(c in string.hexdigits for c in userinput):
			res = int(userinput, 16)
			print("The decimal number of hexadecimal string : " + str(res))
			break
		else:
			raise HexFormatError
	except HexFormatError:
		print("Invalid hexadecimal format")
		print
	except MemoryError:
		print("Input is more than 8 hex digits")
		print